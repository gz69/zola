# Zola

## Screenshot

![Screenshot](/images/screenshot1.png "screenshot1"){: .align-right width="350px"}
![Screenshot](/images/screenshot2.png "screenshot2"){: .align-right width="350px"}
![Screenshot](/images/screenshot3.png "screenshot3"){: .align-right width="350px"}
![Screenshot](/images/screenshot4.png "screenshot4"){: .align-right width="350px"}

## Installation

1. Git clone project into your local repository 

```
git clone https://gitlab.com/gz69/zola.git
```

2. cd zola and run the local server

```
zola serve
```

3. Web server is available at `http://127.0.0.1:1111`


## Options

Customizations exist under a designated `extra.papermod` section.
Refer to [config.toml](config.toml) for available options.
